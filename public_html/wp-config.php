<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'reversekarma' );

/** MySQL database username */
define( 'DB_USER', 'wp' );

/** MySQL database password */
define( 'DB_PASSWORD', 'wp' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',          'm%Oo$HxIVE7<yC6VWD|r+O(v@}];q&f(548fZ$C57OxuiPs+3evUI;MG%QQ=Y<he' );
define( 'SECURE_AUTH_KEY',   ' [de:gS[6$ 3>g1&6`Z*a5GA[NISER1+BXPixK=FvEvVhM,a)E8zC[IC*#P=5HFm' );
define( 'LOGGED_IN_KEY',     't>x;.nN%Pny=z1X@TtCk<;u(=?ExY,#VVJOy?3A@yHqRGj9JVJjYSxp xNhlF51C' );
define( 'NONCE_KEY',         'x$}H$uTJCS+/x&,/MZ]h_4ml>f4lYFdhvjjf$-b?b0q}G63YgX29TB&~OAxnfNOE' );
define( 'AUTH_SALT',         '{ONM/PhZ{u4ArdC.8OBCU-m<A?44Zn-%@Ph_dmzMEpP1se2DUL$wL|-xe1&c)/!1' );
define( 'SECURE_AUTH_SALT',  ':o-0bh6U>,)kRw=lm~/xFo!ZF6Zj=4Pz_;de~KDS;9N{QaDw3&7^(*qrOBd3pu<m' );
define( 'LOGGED_IN_SALT',    'N+qdu}>bRtdt?X(kIa{|3Cstr>J.0o2p7W;u?Zh0MTli+dy#VU|D*n%B^mKzZIh[' );
define( 'NONCE_SALT',        'ul`(V3{.cYTkr.hEFJ3xE$?SQaZA@G`7>AC]]`^ji}]/qfNA*Bw|;hda9xME?|x_' );
define( 'WP_CACHE_KEY_SALT', 'v&hY~LWiP>o&[Xa)V/6ynyHOns[S7ibeq+k[GSGP.2[B[0bk=NR#F{<M<31,u1h!' );

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';


define( 'WP_DEBUG', true );
define( 'SCRIPT_DEBUG', true );


/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) )
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
