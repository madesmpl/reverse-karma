<?php $categories = get_the_category();?>
    <div class="filtr-item col-12 col-sm-4">
    <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
        <div style="background:
        linear-gradient(to bottom, rgba(0,0,0,0) 0%,rgba(0,0,0,0.8) 64%,rgba(0,0,0,0.89) 100%),
        url(<?php the_post_thumbnail_url();?>);">
        <small><strong><?php the_author() ?></strong></small>
        <h3>
                <?php the_title(); ?></h3>
                <div>
                    <span class="cates">
        <?php foreach ( $categories as $category ) {
            if ($category->name != "Blog"):
                printf( '<span class="pill-white">%1$s</span> ',
                esc_html( $category->name ));
            endif;
            }?></span>
            <span class="date"><?php the_time("F d, Y"); ?></span></div>
    </div>
    </a>
    </div>