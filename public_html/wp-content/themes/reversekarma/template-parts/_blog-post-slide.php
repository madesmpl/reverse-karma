<?php $categories = get_the_category();?>
    <div class="slide">
        <div class="background" style="background:
        linear-gradient(to bottom, rgba(0,0,0,0.8) 0%,rgba(0,0,0,0.1) 50%,rgba(0,0,0,0.7) 100%),
        url(<?php the_post_thumbnail_url();?>);"></div>
        <div class="container">
        <strong><?php the_author() ?></strong>
        <h3><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
                <?php the_title(); ?></a></h3>
                <div>
                    <span class="cates">
        <?php foreach ( $categories as $category ) {
            if ($category->name != "Blog"):
                printf( '<span class="pill-white">%1$s</span> ',
                esc_html( $category->name ));
            endif;
            }?></span>
            <span class="date"><?php the_time("F d, Y"); ?></span></div>
        </div>
    </div>