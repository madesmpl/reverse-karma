<?php get_header(); ?>

<main role="main">
	<section id="blog-page-header">
		<div class="slider">
		<div class="owl-rk owl-carousel owl-theme">
		<?php 
		 $args = array(
			'posts_per_page'=>3,
			);
		$the_query = new WP_Query( $args);
		if ( $the_query->have_posts() ) : 
		$titles = array();
		$i=0;
		while ( $the_query->have_posts() ) : $the_query->the_post();
		$titles[$i] = get_the_title();
		get_template_part('template-parts/_blog-post-slide');
		$i++;
		endwhile;?>
		</div>
		<div class="container" style="position:relative;">
		<?php echo "<div id='custom-dots'><ul id='dots' class='owl-dots d-flex'>";
		$s=1;
        foreach($titles as $value){
			echo "<li class='owl-dot'><span class='num'>". $s . "</span><br>" . $value . "</li>";
			$s++;
           }
        echo "</ul></div>";
		?>
		</div>
        </section>
        <?php 
    endif;

?>
		</div>
	</section>

	<section class="container">
		<ul class="multifilter desktop">
			<?php 
			$all_categories = get_categories();
			foreach ( $all_categories as $category ) {
				if ($category->name != "Blog"):
					printf( '<li data-multifilter="%1$s" style="cursor:pointer;" id="%1$s">%2$s</li> ',
						esc_html( $category->slug ),
						esc_html( $category->name )
					);
				endif;
			}?>
			<li data-filter="all" style="cursor:pointer;" id="all">Clear Filters</li>
		</ul>
		<div class="filter-mobile" id="dd">
		<a href="#" id="button">Filter by category</a>
		<ul class="multifilter mobile" id="select">
		<?php 
			$all_categories = get_categories();
			foreach ( $all_categories as $category ) {
				if ($category->name != "Blog"):
					printf( '<li data-multifilter="%1$s" style="cursor:pointer;" id="%1$s">%2$s</li> ',
						esc_html( $category->slug ),
						esc_html( $category->name )
					);
				endif;
			}?>
			<li data-filter="all" style="cursor:pointer;" id="all">Clear Filters</li>
		</div>
		<?php 
		
		if ( have_posts() ) : ?>
		<div class="filter-container my-posts">
			<?php while ( have_posts() ) : the_post();
				get_template_part('template-parts/_blog-post');
			 endwhile; ?>
		</div>
		<div class="loadmore">Load More</div>
		<?php endif; ?>
	</section>
	<!-- /section -->
</main>
<script type="text/javascript">
function GetURLParameter(sParam) {
    var sPageURL = window.location.search.substring(1);
    var sURLVariables = sPageURL.split('&');
    for (var i = 0; i < sURLVariables.length; i++) 
    {
        var sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] == sParam) 
        {
            return sParameterName[1];
        }
    }
}
	var ajaxurl = "<?php echo admin_url( 'admin-ajax.php' ); ?>";
	var page = 2;
	var maxpages = <?php echo $wp_query->max_num_pages;?>;
	jQuery(function ($) {
		var cat = GetURLParameter('category');
		if (cat == null) {
			cat = ''
		}

		// Set up the options
		var options = {
			toggleFilter: cat,
			setupControls: true
		}

		// Call the constructor
		var filterizd = $('.filter-container').filterizr({
			layout: 'sameSize'
		});

		$('#' + cat).click().toggleClass('active');
		$('.multifilter li').not("#all").click(function () {
			$(this).toggleClass('active');
		});
		$('.multifilter li#all').click(function(){
			$('.multifilter li').removeClass('active');
		});        

		
		$(".owl-rk").owlCarousel({
                        loop: true,
                        nav: false,
                        dotsContainer: '#dots',
                        autoplay:true,
						autoplayTimeout:100000,
						autoplayHoverPause:true,
                        items: 1,
                        mouseDrag:false,
						touchDrag:true
                    });
                    $('.owl-dot').click(function () {
                        $(".owl-rk").trigger('to.owl.carousel', [$(this).index(), 300]);
					});
					
		$('body').on('click', '.loadmore', function () {
			var data = {
				'action': 'load_posts_by_ajax',
				'page': page,
				'security': '<?php echo wp_create_nonce("load_more_posts"); ?>'
			};
			$(".loadmore").addClass("loader");
			$.post(ajaxurl, data, function (response) {

				$('.my-posts').append(response);
				var className = document.getElementsByClassName('active');
				var classnameCount = className.length;
				var IdStore = new Array();
				for (var j = 0; j < classnameCount; j++) {
					IdStore.push(className[j].id);
				}

				var filterizd = $('.filter-container').filterizr({
					layout: 'sameSize'
				});
				var arrayLength = IdStore.length;
				for (var i = 0; i < arrayLength; i++) {
					$('#' + IdStore[i]).click().toggleClass('active');
					console.log(IdStore[i] + "clicked");
				}
				page++;
				if (page > maxpages) {
					$(".loadmore").remove();
				}
				else{
					$(".loadmore").removeClass("loader");
				}
			});
		});

	});

</script>
<?php get_footer(); ?>
