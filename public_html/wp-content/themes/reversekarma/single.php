<?php get_header(); ?>

	<main role="main">
	

		

	<?php if (have_posts()): while (have_posts()) : the_post(); 
	$categories = get_the_category();?>

		<!-- article -->
		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		<section id="post-header">
			<div class="container">
				<a href="<?php get_site_url();?>/blog" class="back">Back to all posts</a>
		<h1><?php the_title(); ?></h1>
		<hr>
		<div class="category-time">
		<?php foreach ( $categories as $category ) {
			if ($category->name != "Blog"):
                echo'<span class="pill-white">' . $category->name . '</span> ';
            endif;
            }?>
			<span class="date"><?php the_time('F j, Y'); ?></span>
		</div>
		</div>
	</section>
		<section class="container">
			<!-- /post details -->
			
			<?php the_content(); // Dynamic Content ?>

			<?php the_tags( __( 'Tags: ', 'html5blank' ), ', ', '<br>'); // Separated by commas with a line break at the end ?>



			<?php edit_post_link(); // Always handy to have Edit Post Links available ?>

		</article>
		<!-- /article -->

	<?php endwhile; ?>

	<?php endif; ?>

	</section>
	<!-- /section -->
	</main>

<?php get_footer(); ?>
