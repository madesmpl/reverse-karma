<?php if ( have_rows( 'contact', 5 ) ) : ?>
<section id="support">
	<?php while ( have_rows( 'contact', 5 ) ) : the_row(); ?>
	<h3><small>
			<?php the_sub_field( 'top_headline' ); ?></small><br><br>
		<a href="mailto:<?php the_sub_field( 'email' ); ?>" class="email">
			<?php the_sub_field( 'email' ); ?></a></h3>

	<?php $button_link = get_sub_field( 'button_link' ); ?>
	<?php if ( $button_link ) { ?>
	<a href="<?php echo $button_link; ?>" class="btn-white  btn-normal">
		<span><?php the_sub_field( 'button_text' ); ?></a></span>
	<?php } ?>
	<?php endwhile; ?>
</section>
<?php endif; ?>
<!-- footer -->
<footer class="footer" role="contentinfo">
	<div class="container">
		<div class="row">
			<div class="col-12 col-lg-4">
				<div class="footer-logo logo-col">
					<a href="<?php echo home_url(); ?>">
						<!-- svg logo - toddmotto.com/mastering-svg-use-for-a-retina-web-fallbacks-with-png-script -->
						<img src="<?php echo get_template_directory_uri(); ?>/assets/img/logo.svg" alt="Reverse Karma" class="logo-img">
					</a>
				</div>
			</div>
			<div class="col-12 col-md-6 col-lg-4">
				<div class="row">
				<div class="col-5 offset-1 col-sm-4 offset-sm-2">
				<?php
					wp_nav_menu( array(
						'theme_location' => 'footer-left-menu',
					) );
					?>
				</div>
				<div class="col-5 col-sm-4">
				<?php
					wp_nav_menu( array(
						'theme_location' => 'footer-right-menu',
					) );
					?>
				</div>
					</div>
			</div>
			<div class="col-12 col-md-6 col-lg-4 social-col">
				<div class="social">
					<a href="https://www.instagram.com/reverseyourkarma"><i class="icon-instagram"></i></a>
					<a href="https://www.facebook.com/reverseyourkarma"><i class="icon-facebook"></i></a> 
				</div>
				<p class="credit">Built by <a href="https://madesmpl.com">Madesmpl</a></p>
			</div>
		</div>
		<div class="row copyright">
			<div class="col-12 col-md-6">
				<?php echo date('Y');?> © Reverse Karma | All rights reserved.
			</div>
		</div>
	</div>
</footer>
<!-- /footer -->

</div>
<!-- /wrapper -->

<?php wp_footer(); ?>
<!-- Hotjar Tracking Code for www.reversekarma.com -->
<script>
   (function(h,o,t,j,a,r){
       h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
       h._hjSettings={hjid:1236675,hjsv:6};
       a=o.getElementsByTagName('head')[0];
       r=o.createElement('script');r.async=1;
       r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
       a.appendChild(r);
   })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
</script>
<!-- analytics -->
<script>
	(function (f, i, r, e, s, h, l) {
		i['GoogleAnalyticsObject'] = s;
		f[s] = f[s] || function () {
			(f[s].q = f[s].q || []).push(arguments)
		}, f[s].l = 1 * new Date();
		h = i.createElement(r),
			l = i.getElementsByTagName(r)[0];
		h.async = 1;
		h.src = e;
		l.parentNode.insertBefore(h, l)
	})(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');
	ga('create', 'UA-XXXXXXXX-XX', 'yourdomain.com');
	ga('send', 'pageview');

</script>
</body>

</html>
