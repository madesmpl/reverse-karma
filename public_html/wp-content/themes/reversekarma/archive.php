<?php get_header(); ?>

	<main role="main">
	<section id="page-header">
		<div class="text">
		<h1><?php _e( 'Archives', 'html5blank' ); ?></h1>
		</div>
		</section>

		<section class="container">

			<?php echo do_shortcode('[ajax_load_more]');?>

		</section>
		<!-- /section -->
	</main>

<?php get_sidebar(); ?>

<?php get_footer(); ?>
