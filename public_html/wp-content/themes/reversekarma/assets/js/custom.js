"use strict";

/**
 * Custom JavaScript
 *
 * @since 1.0.0
 */
var sub = function sub(a, b) {
  return a - b;
};
"use strict";

/**
 * Custom JavaScript
 *
 * @since 1.0.0
 */
// Get URL parameters
AOS.init({
  mirror: false,
  once: true,
  duration: 1900
});
jQuery(document).ready(function ($) {
  $('.menu-toggle').click(function () {
    $('.mobile-nav').fadeToggle("fast");
    $(this).toggleClass('is-active');
  });
  $(window).scroll(function () {
    var threshold = 300; // number of pixels before bottom of page that you want to start fading

    var op = ($(document).height() - $(window).height() - $(window).scrollTop()) / threshold / 2;

    if (op <= 1) {
      $(".scroll-down-line").fadeOut(200);
    } else {
      $(".scroll-down-line").fadeIn(200);
    }
  });
  $(document).click(function (e) {
    var target = e.target;

    if (!$(target).is('.menu-toggle') && !$(target).parents().is('.menu-toggle')) {
      $('.mobile-nav').fadeOut("fast");
      $('.menu-toggle').removeClass('is-active');
    }
  });
  $(function () {
    $('#select').click(function () {
      $('#sel-option').show();
    });
    $('#sel-option a').click(function (e) {
      $('#select').text($(this).text());
      $('#sel-option').hide();
      $(this).addClass('current');
      e.preventDefault();
    });
  });
});