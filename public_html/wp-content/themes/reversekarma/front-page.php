<?php get_header(); ?>
<?php if ( have_rows( 'masthead' ) ) : ?>
<section id="masthead">
	<div class="palm palm-1" style="background-image:url(<?php echo get_template_directory_uri(); ?>/assets/img/4.png);"></div>
	<div class="palm palm-2" style="background-image:url(<?php echo get_template_directory_uri(); ?>/assets/img/2.png);"></div>

	<div class="text">
		<?php while ( have_rows( 'masthead' ) ) : the_row(); ?>

		<h1><small data-aos="fade-up">
				<?php the_sub_field( 'top_headline' ); ?></small><br />
			<span data-aos="fade-down">
				<?php the_sub_field( 'bottom_headline' ); ?></span>
		</h1>
		<a class="btn btn-normal" href="<?php the_sub_field( 'button_link' ); ?>" data-aos="fade-bottom" data-aos-delay="1000">
			<span>
				<?php the_sub_field( 'button_text' ); ?></a></span>
		<?php endwhile; ?>
	</div>
</section>
<?php endif; ?>

<?php if ( have_rows( 'story' ) ) : ?>
<section id="story" style="background-image:url(<?php echo get_template_directory_uri(); ?>/assets/img/story-background.png)">
	<?php while ( have_rows( 'story' ) ) : the_row(); ?>
	<div class="container">
		<div class="row">
			<div class="image col-12 offset-lg-1 col-lg-5">
				<?php $image = get_sub_field( 'image' ); ?>
				<?php if ( $image ) { ?>
				<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" data-aos="fade-up" />
				<?php } ?>
			</div>
			<div class="text offset-lg-1 col-lg-5" data-aos="fade-left" data-aos-delay="300">
				<h2><small>
						<?php the_sub_field( 'bottom_headline' ); ?></small><br />
					<?php the_sub_field( 'top_headline' ); ?>
				</h2>
				<hr class="small-line" />
				<p>
					<?php the_sub_field( 'text' ); ?>
				</p><br />

				<?php $button_link = get_sub_field( 'button_link' ); ?>
				<?php if ( $button_link ) { ?>
				<a href="<?php echo $button_link; ?>" class="btn btn-normal">
					<span>
						<?php the_sub_field( 'button_text' ); ?></span></a>
				<?php } ?>
			</div>
		</div>
	</div>
	<?php endwhile; ?>
</section>
<?php endif; ?>

<section id="products">
	<div class="palm palm-3" style="background-image:url(<?php echo get_template_directory_uri(); ?>/assets/img/4.png);"></div>
	<div class="palm palm-4" style="background-image:url(<?php echo get_template_directory_uri(); ?>/assets/img/5.png);"></div>

	<h3 data-aos="fade-down">Top Sellers<br /><small>Select a Product</small></h3>
	<div class="container">
		<div class="row">
			<?php
if( have_rows('products') ):
	$p_time = 300;
    while ( have_rows('products') ) : the_row();?>
			<a href="<?php the_sub_field( 'link' ); ?>" class="product-item col-12 col-lg-4">
				<div data-aos="fade-up" data-aos-delay="<?php echo $p_time;?>">
					<div class="image">
						<?php $image = get_sub_field( 'image' ); ?>
						<?php if ( $image ) { ?>
						<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
						<?php } ?>
					</div>
					<div class="text">
						<h2><small>
								<?php the_sub_field('top_headline');?></small><br />
							<?php the_sub_field('bottom_headline');?>
						</h2>
						<div class="btn-outline-white btn-small"><span>Shop Now</span></div>
					</div>
				</div>
			</a>
			<?php
			$p_time = $p_time + 300; endwhile;
    endif;
?>
		</div>
	</div>
</section>
<?php if ( have_rows( 'cta' ) ) : ?>
<section id="cta">
	<div class="container">
		<div class="row">
			<?php while ( have_rows( 'cta' ) ) : the_row(); ?>
			<div class="text col-12 col-lg-6 offset-lg-1">
				<?php the_sub_field( 'text' ); ?>
			</div>
			<div class="link col-12 col-lg-4">
				<a href="<?php the_sub_field( 'button_link' ); ?>" class="btn btn-normal">
					<span>
						<?php the_sub_field( 'button_text' ); ?></a></span>
			</div>
			<?php endwhile; ?>
		</div>
	</div>
</section>
<?php endif; ?>
<?php if ( have_rows( 'blog' ) ) : 

 while ( have_rows( 'blog' ) ) : the_row(); 
$url = get_site_url();?>
<section id="blog">
	<h3 data-aos="fade-down">
		<?php the_sub_field( 'header' ); ?><br /><small>
			<?php the_sub_field( 'sub_header' ); ?></small></h3>
	<div class="container">
		<div class="row">
			<?php 
			$args = array(
				'post_type' => 'post',
				'post_status' => 'publish',
				'posts_per_page' => '3',
			);
			$front_posts = new WP_Query( $args );
			if ( $front_posts->have_posts() ) :
				$b_time = 300;
				?>
			<?php while ( $front_posts->have_posts() ) : $front_posts->the_post() ?>
			<?php $categories = get_the_category();?>
			<div class="filtr-item col-12 col-md-6 col-lg-4" data-aos="fade-up" data-aos-delay="<?php echo $b_time;?>">
				<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
					<div style="background:
        linear-gradient(to bottom, rgba(0,0,0,0) 0%,rgba(0,0,0,0.8) 64%,rgba(0,0,0,0.89) 100%),
        url(<?php the_post_thumbnail_url();?>);">
						<small><strong>
								<?php the_author() ?></strong></small>
						<h3>
							<?php the_title(); ?>
						</h3>
						<div>
							<span class="cates">
								<?php foreach ( $categories as $category ) {
            if ($category->name != "Blog"):
                printf( '<span class="pill-white">%1$s</span> ',
                esc_html( $category->name ));
            endif;
            }?></span>
							<span class="date">
								<?php the_time("F d, Y"); ?></span></div>
					</div>
				</a>
			</div>
			<?php 
				$b_time = $b_time + 300; endwhile;
			endif;
			wp_reset_query();
			?>
		</div>
		<div class="row cta" data-aos="fade-up" data-aos-delay="500">
			<div class="col-12 col-lg-6 offset-lg-1 text">
				<?php the_sub_field( 'blog_cta_text' ); ?>

			</div>
			<div class="col-12 col-lg-3 link">
				<a href="<?php get_site_url();?>/blog" class="btn btn-normal"><span>
						<?php the_sub_field( 'button_text' ); ?></span></a>
			</div>
		</div>
	</div>
</section>
<?php endwhile; ?>
<?php endif; ?>

<section id="featured">
	<div class="palm palm-6" style="background-image:url(<?php echo get_template_directory_uri(); ?>/assets/img/1.png);"></div>

	<h3 data-aos="fade-down">
		<?php the_field( 'featured_header' ); ?><br /><small>
			<?php the_field( 'featured_subtitle' ); ?></small></h3>
	<div class="container">
		<div class="row">
			<?php
if( have_rows('featured_images') ):
	$f_time = 300;
    while ( have_rows('featured_images') ) : the_row();?>
			<?php $image = get_sub_field( 'image' ); ?>
			<?php if ( $image ) { ?>
			<a href="<?php the_sub_field( 'link' ); ?>" class="image col-12 col-md" data-aos="fade-up"
			 data-aos-delay="<?php echo $f_time;?>"><img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" /></a>
			<?php } ?>
			<?php $f_time = $f_time + 300; endwhile;
    endif;
?>
		</div>
	</div>
</section>

<?php get_footer(); ?>
