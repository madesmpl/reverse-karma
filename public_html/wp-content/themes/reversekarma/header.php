<!doctype html>
<html <?php language_attributes(); ?> class="no-js">

<head>
	<meta charset="<?php bloginfo('charset'); ?>">
	<title>
		<?php wp_title(''); ?>
	</title>

	<link href="//www.google-analytics.com" rel="dns-prefetch">
	<link href="<?php echo get_template_directory_uri(); ?>/img/icons/favicon.ico" rel="shortcut icon">
	<link href="<?php echo get_template_directory_uri(); ?>/img/icons/touch.png" rel="apple-touch-icon-precomposed">

	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="<?php bloginfo('description'); ?>">
	<link rel="apple-touch-icon" sizes="180x180" href="<?php echo get_template_directory_uri(); ?>/apple-touch-icon.png">
<link rel="icon" type="image/png" sizes="32x32" href="<?php echo get_template_directory_uri(); ?>/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="16x16" href="<?php echo get_template_directory_uri(); ?>/favicon-16x16.png">
<link rel="manifest" href="<?php echo get_template_directory_uri(); ?>/site.webmanifest">
<link rel="mask-icon" href="<?php echo get_template_directory_uri(); ?>/safari-pinned-tab.svg" color="#5bbad5">
<meta name="msapplication-TileColor" content="#000000">
<meta name="theme-color" content="#000000">
<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s)
{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};
if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];
s.parentNode.insertBefore(t,s)}(window,document,'script',
'https://connect.facebook.net/en_US/fbevents.js');
 fbq('init', '582032728966888'); 
fbq('track', 'PageView');
</script>
<noscript>
 <img height="1" width="1" 
src="https://www.facebook.com/tr?id=582032728966888&ev=PageView
&noscript=1"/>
</noscript>
<!-- End Facebook Pixel Code -->
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

	<div class="scroll-down-line">
		<p>Scroll Down</p>
	</div>
	<div class="social-line">
		<div class="socials">
	<a href="https://www.instagram.com/reverseyourkarma"><i class="icon-instagram"></i></a>
		<a href="https://www.facebook.com/reverseyourkarma"><i class="icon-facebook"></i></a>
</div>
		<p><a href="<?php echo get_site_url();?>/support">Get In Touch</a></p>
		
	</div>
	<!-- header -->
	<header class="header" role="banner">
	<div class="logo d-block d-lg-none">
				<a href="<?php echo home_url(); ?>">
					<!-- svg logo - toddmotto.com/mastering-svg-use-for-a-retina-web-fallbacks-with-png-script -->
					<img src="<?php echo get_template_directory_uri(); ?>/assets/img/logo2.png" alt="Reverse Karma" class="logo-img">
				</a>
			</div>
		<nav class="nav d-none d-lg-flex" role="navigation">
			<?php
					wp_nav_menu( array(
						'theme_location' => 'header-left-menu',
						'menu_id'        => 'primary-left-menu',
					) );
					?>
			<div class="logo d-none d-lg-inline-flex">
				<a href="<?php echo home_url(); ?>">
					<!-- svg logo - toddmotto.com/mastering-svg-use-for-a-retina-web-fallbacks-with-png-script -->
					<img src="<?php echo get_template_directory_uri(); ?>/assets/img/logo2.png" alt="Reverse Karma" class="logo-img">
				</a>
			</div>
			<?php
					wp_nav_menu( array(
						'theme_location' => 'header-right-menu',
						'menu_id'        => 'primary-right-menu',
					) );
					?>
		</nav>
		<button class="menu-toggle d-block d-lg-none hamburger hamburger--collapse" type="button">
  <span class="hamburger-box">
    <span class="hamburger-inner"></span>
  </span>
</button>
	</header>
	<div class="mobile-nav">
            <?php
					wp_nav_menu( array(
						'theme_location' => 'header-mobile-menu',
						'menu_id'        => 'primary-mobile-menu',
					) );
					?>
        </div>
	<div class="wrapper">
