<?php get_header(); ?>

	<main role="main">
		<!-- section -->
		<section class="error-page">
		<div class="text">

		<h1><span>404</span><br />
				<small>
		Um... I think you're lost</small>
		</h1>
		<a class="btn btn-normal" href="<?php get_site_url(); ?>">
			<span>
				Return to Safety</a></span>
		</div>

		</section>
		<!-- /section -->
	</main>


<?php get_footer(); ?>
