<?php 
/**
* Template Name: About Page
*/
get_header(); ?>
<div class="palm palm-3" style="background-image:url(<?php echo get_template_directory_uri(); ?>/assets/img/4.png);"></div>
	
<main role="main">
	<?php if (have_posts()): while (have_posts()) : the_post(); ?>
	<section id="page-header-about">
		<div class="container">
		<div class="row">
			<div class="col-12 col-lg-5">
				<h1>
					<?php the_field( 'display_title' ); ?>
				</h1>
				<h4><?php the_content(); ?></h4>
			</div>
			<div class="col-12 col-lg-6 offset-lg-1 text">
				<div><?php the_field( 'top_right_text' ); ?></div>
			</div>
		</div>
		</div>
	</section>
	<?php $bg_img = get_field( 'bg_img' ); ?>
	<?php if ( have_rows( 'section_1' ) ) : ?>
	<section id="who" style="background-image:url(<?php echo $bg_img['url']; ?>);">
	
		<div class="container">
		<?php while ( have_rows( 'section_1' ) ) : the_row(); ?>
		<div class="text-center">
			<h2 data-aos="fade-down">
				<?php the_sub_field( 'title' ); ?><br/>
				<small>
					<?php the_sub_field( 'subtitle' ); ?></small></h2>
		</div>
		<div class="row">
			<div class="col-md-5 offset-md-1 text" data-aos="fade-up" data-aos-delay="400">
				<?php the_sub_field( 'column_1' ); ?>
			</div>
			<div class="col-md-5 text" data-aos="fade-up" data-aos-delay="800">
				<?php the_sub_field( 'column_2' ); ?>
			</div>
		</div>
		<?php endwhile; ?>
		</div>
	</section>
	<?php endif; ?>

	<?php if ( have_rows( 'section_2' ) ) : ?>
	<section id="how">
	<div class="palm palm-4" style="background-image:url(<?php echo get_template_directory_uri(); ?>/assets/img/5.png);"></div>
	<div class="container">
		<?php while ( have_rows( 'section_2' ) ) : the_row(); ?>
		<div class="row">
			<div class="col-12 col-lg-6 d-flex align-items-center" data-aos="fade-up">
				<?php $image = get_sub_field( 'image' ); ?>
				<?php if ( $image ) { ?>
				<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
				<?php } ?>
			</div>
			<div class="col-12 col-lg-6 d-flex flex-column justify-content-center" data-aos="fade-left" data-aos-delay="300">
				<h2>
					<?php the_sub_field( 'title' ); ?><br/>
					<small>
						<?php the_sub_field( 'subtitle' ); ?></small></h2>
				<div class="text"><?php the_sub_field( 'text' ); ?></div>
			</div>
		</div>
		<?php endwhile; ?>
		</div>
	</section>
	<?php endif; ?>

	<?php if ( have_rows( 'section_3' ) ) : ?>
	<section id="why">
	<div class="palm palm-6" style="background-image:url(<?php echo get_template_directory_uri(); ?>/assets/img/1.png);"></div>
	<div class="container">
		<?php while ( have_rows( 'section_3' ) ) : the_row(); ?>
		<div class="row flex-column-reverse flex-lg-row">
			<div class="col-12 col-lg-6 d-flex flex-column justify-content-center" data-aos="fade-right" data-aos-delay="300">
				<h2>
					<?php the_sub_field( 'title' ); ?><br/>
					<small>
						<?php the_sub_field( 'subtitle' ); ?></small></h2>
						<div class="text"><?php the_sub_field( 'text' ); ?></div>
			</div>
			<div class="col-12 col-lg-6 d-flex align-items-center" data-aos="fade-up">
				<?php $image = get_sub_field( 'image' ); ?>
				<?php if ( $image ) { ?>
				<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
				<?php } ?>
			</div>
		</div>
		<?php endwhile; ?>
				</div>
	</section>
	<?php endif; ?>
	<?php endwhile; ?>
	<?php endif; ?>

	</section>
	<!-- /section -->
</main>
<?php get_footer(); ?>
