<?php get_header(); ?>

	<main role="main">
	<section id="page-header">
		<div class="text">
		<h1><?php single_cat_title(); ?></h1>
		</div>
		</section>

		<section class="container">

			<?php if(is_category()){
   $cat = get_query_var('cat');
   $category = get_category ($cat);
   echo '<h1>'.$category->cat_name.'</h1>';
   echo do_shortcode('[ajax_load_more category="'.$category->slug.'" cache="true" cache_id="cache-'.$category->slug.'"]');
} ?>

		</section>
		<!-- /section -->
	</main>

<?php get_sidebar(); ?>

<?php get_footer(); ?>
