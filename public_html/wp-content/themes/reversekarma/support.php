<?php 
/**
* Template Name: Support Page
*/
get_header(); ?>

<main role="main">
	<!-- section -->
	<?php if (have_posts()): while (have_posts()) : the_post(); ?>
	<section id="support-page-header">
	<div class="palm palm-4" style="background-image:url(<?php echo get_template_directory_uri(); ?>/assets/img/5.png);"></div>
		<div class="text">
			<h1>
			<span><?php the_field( 'title' ); ?></span><br /><br class="d-block d-md-none"/>
				<small>
					<?php the_field( 'subtitle' ); ?></small>
			</h1>
		</div>
	</section>

	<section id="section-1" class="container z-index-2">
	
		<div class="row">
			<div class="col-12 col-md-5 offset-md-1 z-index-2" data-aos="fade-up" data-aos-delay="200">
				<?php the_field( 'column_1' ); ?>
			</div>
			<div class="col-12 col-md-5 z-index-2" data-aos="fade-up" data-aos-delay="400">
				<?php the_field( 'column_2' ); ?>
			</div>
		</div>
	</section>
	<section id="section-2">
	<div class="palm palm-3" style="background-image:url(<?php echo get_template_directory_uri(); ?>/assets/img/4.png);"></div>
		<div class="container" >
		<?php if ( have_rows( 'box_1' ) ) : ?>
	<?php while ( have_rows( 'box_1' ) ) : the_row(); ?>
	<a href="mailto:<?php the_sub_field( 'email' ); ?>" data-aos="fade-up" data-aos-delay="200">
	<div class="box">
		<?php $icon = get_sub_field( 'icon' ); ?>
		<div class="image">
		<?php if ( $icon ) { ?>
			<img src="<?php echo $icon['url']; ?>" alt="<?php echo $icon['alt']; ?>" />
		<?php } ?>
		</div>
		<h2><?php the_sub_field( 'title' ); ?></h2>
		<small><?php the_sub_field( 'subtitle' ); ?></small>
		<span class="email"><?php the_sub_field( 'email' ); ?></span>
		</div>
		</a>
	<?php endwhile; ?>
<?php endif; ?>
			
<?php if ( have_rows( 'box_2' ) ) : ?>
	<?php while ( have_rows( 'box_2' ) ) : the_row(); ?>
	<a href="mailto:<?php the_sub_field( 'email' ); ?>" data-aos="fade-up" data-aos-delay="400">
	<div class="box">
		<?php $icon = get_sub_field( 'icon' ); ?>
		<div class="image">
		<?php if ( $icon ) { ?>
			<img src="<?php echo $icon['url']; ?>" alt="<?php echo $icon['alt']; ?>" />
		<?php } ?>
		</div>
		<h2><?php the_sub_field( 'title' ); ?></h2>
		<small><?php the_sub_field( 'subtitle' ); ?></small>
		<span class="email"><?php the_sub_field( 'email' ); ?></span>
		</div>
		</a>
	<?php endwhile; ?>
<?php endif; ?>

		</div>
	</section>
	<section id="faqs">
		<div class="container">
			<div class='row'>
				<div class="col" data-aos="fade-up">
					<h2>
						<span><?php the_field( 'faq_header' ); ?></span><br /><small>
							<?php the_field( 'faq_subtitle' ); ?></small></h2>
				</div>
			</div>
		</div>
		<div class="container accordion">
		<div class="row">
				<div id="accordion">
					<?php

			// check if the repeater field has rows of data
			if( have_rows('faqs') ):

				// loop through the rows of data
				while ( have_rows('faqs') ) : the_row();?>

					<h4 class="accordion-toggle">
						<div>
							<?php the_sub_field('question'); ?>
						</div>
					</h4>
					<div class="accordion-content">
						<?php the_sub_field('answer'); ?>
					</div>
					<?php endwhile;

			endif;

?>
				</div>
			</div>
		</div>
	</section>
	<?php endwhile; ?>
	<?php endif; ?>
</main>
<script type="text/javascript">
	jQuery(document).ready(function ($) {
		$('#accordion').find('.accordion-toggle').click(function () {

			//Expand or collapse this panel
			$(this).next().slideToggle('fast');
			$(this).toggleClass('opened');
			//Hide the other panels
			$(".accordion-content").not($(this).next()).slideUp('fast');
			$(".accordion-toggle").not($(this)).removeClass('opened');
		});
	});

</script>
<?php get_footer(); ?>
