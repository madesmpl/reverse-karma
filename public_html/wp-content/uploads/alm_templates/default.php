<?php $categories = get_categories( array(
    'orderby' => 'name',
    'parent'  => 0
) );?>
<div <?php if (!has_post_thumbnail()) { ?> class="no-img filtr-item" data-category="<?php foreach ( $categories as $category ) {
    printf( '%2$s, ',
        esc_url( get_category_link( $category->term_id ) ),
        esc_html( $category->term_id )
    );
}?>"<?php } ?>>
<?php foreach ( $categories as $category ) {
    printf( '<a href="%1$s">%2$s</a> ',
        esc_url( get_category_link( $category->term_id ) ),
        esc_html( $category->name )
    );
}?>
   <?php if ( has_post_thumbnail() ) { the_post_thumbnail('alm-thumbnail'); }?>
   <h3><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h3>
   <p class="entry-meta"><?php the_time("F d, Y"); ?></p>
   <?php the_excerpt(); ?>
</div>